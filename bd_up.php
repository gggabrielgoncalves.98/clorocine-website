<?php

  $bd = new SQLite3("filmes.db");
  
  
  $sql = "DROP TABLE IF EXISTS filmes";
  if ($bd->exec($sql))
     echo "\ntabela filmes apagada\n";


  $sql = "CREATE TABLE filmes(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    titulo VARCAR(200) NOT NULL,
    poster image,
    sinopse TEXT,
    nota DECIMAL(2,1)
    )
    ";

    if ($bd->exec($sql))
       echo "\ntabela filmes criada\n";
       
    else
    echo "\nerro ao criar tabela filmes \n";


   //  $sql = "INSERT INTO filmes(id, titulo, poster, sinopse, nota) VALUES (
   //    0,
   //    'Vingadores',
   //    'https://www.themoviedb.org/t/p/original/q6725aR8Zs4IwGMXzZT8aC8lh41.jpg',
   //    'Após os eventos devastadores de Vingadores: Guerra Infinita, o universo está em ruínas devido aos esforços do Titã Louco, Thanos',
   //    '8.6'     
      
   //    ) ";
   // if ($bd->exec($sql))
   // echo "\nfilmes inseridos com sucesso\n";
   // else
   // echo "\nerro ao inserir filmes\n";

   $sql = "INSERT INTO filmes(id, titulo, poster, sinopse, nota) VALUES (
   1,
   'Jogo da Imitação',
   'https://www.themoviedb.org/t/p/original/4AfY1emExCH1HRDn01rs82gzmoj.jpg',
   'Em 1939, a recém-criada agência de inteligência britânica MI6 recruta Alan Turing,um aluno da Universidade de Cambridge, para entender códigos nazistas, incluindo o Enigma, que criptógrafos acreditavam ser inquebrável.A equipe de Turing, incluindo Joan Clarke, analisa as mensagens de Enigma, enquanto ele constrói uma máquina para decifrá-las.',
   8.0     
   
   ) ";
   if ($bd->exec($sql))
   echo "\nfilmes inseridos com sucesso\n";
   else
   echo "\nerro ao inserir filmes\n";

?>

