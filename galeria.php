<?php include "cabecalho.php" ?>

<?php

$bd = new SQLite3("filmes.db");
$sql = "SELECT * FROM filmes";
$filmes = $bd->query($sql);


$filme1 = [
  "titulo" => "Vingadores",
  "nota" => 8.6,
  "sinopse" => "Após os eventos devastadores de 'Vingadores:
Guerra Infinita', o universo está em ruínas devido aos esforços
do Titã Louco, Thanos",
  "poster" => "https://www.themoviedb.org/t/p/original/q6725aR8Zs4IwGMXzZT8aC8lh41.jpg"
];
$filme2 = [
  "titulo" => "Ad Astra",
  "nota" => 6.1,
  "sinopse" => "Roy McBride é um engenheiro espacial, portador de um leve grau de autismo, 
que decide empreender a maior jornada de sua vida: viajar para o espaço, cruzar a galáxia 
e tentar descobrir o que aconteceu com seu pai,
 um astronauta que se perdeu há vinte anos atrás no caminho para Netuno.",
  "poster" => "https://www.themoviedb.org/t/p/original/wigZBAmNrIhxp2FNGOROUAeHvdh.jpg"
];
$filme3 = [
  "titulo" => "Jogo da Imitação",
  "nota" => 8.0,
  "sinopse" => "Em 1939, a recém-criada agência de inteligência britânica MI6 recruta Alan Turing,
um aluno da Universidade de Cambridge, para entender códigos nazistas, incluindo o 'Enigma', que criptógrafos acreditavam ser inquebrável.
A equipe de Turing, incluindo Joan Clarke, analisa as mensagens de 'Enigma', enquanto ele constrói uma máquina para decifrá-las.",
  "poster" => "https://www.themoviedb.org/t/p/original/4AfY1emExCH1HRDn01rs82gzmoj.jpg"
];
$filme4 = [
  "titulo" => "Star Wars: Episodio IX - A Ascenção de Skywalker",
  "nota" => 6.4,
  "sinopse" => "Com o retorno do Imperador Palpatine, todos voltam a temer seu poder e, com isso,
a Resistência toma a frente da batalha que ditará os rumos da galáxia. 
Treinando para ser uma completa Jedi, Rey (Daisy Ridley) ainda se encontra em conflito com seu passado",
  "poster" => "https://www.themoviedb.org/t/p/original/3mTyM2kywFuDmq3RQa2TFIpVCHt.jpg"
];
// $filmes = [$filme1, $filme2, $filme3, $filme4];
?>


<body>
  <nav class="nav-extended  purple lighten-3">
    <div class="nav-wrapper">
      <ul id="nav-mobile" class="right">
        <li class="active"><a href="galeria.php">Galeria</a></li>
        <li><a href="cadastrar.php">Cadastrar</a></li>
      </ul>
    </div>
    <div class="nav-header center">
      <h1>CLOROCINE</h1>
    </div>
    <div class="nav-content">
      <ul class="tabs tabs-transparent  purple darken-1">
        <li class="tab"><a class="active" href="#test1">Todos</a></li>
        <li class="tab"><a href="#test2">Assistidos</a></li>
        <li class="tab"><a href="#test3">Favoritos</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <?php while ($filme = $filmes->fetchArray()) :  ?>
        <div class="col s12 m6 l3">
          <div class="card hoverable">
            <div class="card-image">
              <img src="<?= $filme["poster"] ?>" />

              <a class="btn-floating halfway-fab waves-effect waves-lightred">
                <i class="material-icons purple darken-1 ">favorite_border</i>
              </a>

            </div>
            <div class="card-content">
              <p class="valign-wrapper">
                <i class="material-icons amber-text ">star</i> <?= $filme["nota"] ?>
              </p>
              <span class="card-title"><?= $filme["titulo"] ?></span>
              <p><?= $filme["sinopse"] ?></p>
            </div>
          </div>
        </div>
      <?php endwhile ?>
    </div>
  </div>
</body>
<?php if(isset($_GET)) :?>
<script>
    M.tost({
      html: '<?=$_GET["msg"]?>'
    });
</script>

<?php endif ?>

</html>